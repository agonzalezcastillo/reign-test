const mongoose = require('mongoose');

const PostSchema = mongoose.Schema({
  id: { type: Number, unique: true },
  title: { type: String, required: false },
  url: { type: String, required: false },
  story_url: { type: String, required: false },
  author: { type: String, required: false },
  points: { type: Number, required: false },
  story_text: { type: String, required: false },
  comment_text: { type: String, required: false },
  num_comments: { type: Number, required: false },
  story_id: { type: Number, required: false },
  story_title: { type: String, required: false },
  date: { type: Date, default: Date.now },
  deleted: { type: Boolean, default: false }
});

PostSchema.path('id').index({ unique: true });

module.exports = mongoose.model('Post', PostSchema);
