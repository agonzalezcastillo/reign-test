const express = require('express');
const router = express.Router();
const Post = require('../../models/Post');
const request = require('request');

router.get('/', (req, response) => {
  let posts;
  const postsArray = [];
  let hits;
  try {
    request(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
      (err, body) => {
        posts = JSON.parse(body.body);
        hits = posts.hits;

        hits.forEach(element => {
          const post = new Post({
            title: element.title,
            story_title: element.story_title,
            url: element.url,
            id: element.objectID,
            story_url: element.story_url,
            description: element.description,
            author: element.author,
            date: element.created_at
          });
          postsArray.push(post);
          post
            .save()
            .then(data => {})
            .catch(err => {
              console.error('request failed:', err);
            });
        });

        response.status(200).send(postsArray);
        if (err) {
          console.error('request failed:', err);
        }
      }
    );
  } catch (err) {}
});

module.exports = router;
