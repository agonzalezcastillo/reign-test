const express = require('express');
const router = express.Router();
const Post = require('../../models/Post');

router.patch('/:postId', async (req, res) => {
  try {
    const post = await Post.updateOne(
      { id: req.params.postId },
      { $set: { deleted: true } }
    );

    res.json(post);
  } catch (err) {
    res.json({ message: err });
  }
});

router.get('/', async (req, res) => {
  try {
    const posts = await Post.find({ deleted: false });   
    posts.sort((a,b)=>b.date - a.date); 
    res.json(posts);    
  } catch (err) {
    res.json({ message: err });
  }
});

router.post('/', (req, res) => {
  const post = new Post({
    title : req.body.title,
    story_title : req.body.story_title,
    url : req.body.url,
    id : req.body.id,
    story_url : req.body.story_url,
    author : req.body.author,
    date : req.body.date,
  });  
  post
    .save()
    .then(data => {
      res.status(200).json(data);
    })
    .catch(err => {
      res.json({ message: err });
    });
});

module.exports = router;
