process.env.NODE_ENV = 'test';

const expect = require("chai").expect;
const request = require("supertest");

const app = require("../../../app.js");
const conn = require("../../../db/index");

describe("POST /posts", () => {
  before(done => {
    conn.connect().then(() => done())
    .catch((err) => done(err));
  });

  after(done => {
    conn.close().then(() => done())
    .catch((err) => done(err));
  });

  it("OK creating a post", done => {
    request(app)
      .post("/posts")
      .send({
        title: null,
        story_title: "The Eternal Novice Trap",
        url: null,
        id: 21908596,
        story_url: "https://feoh.org/2019/12/27/the-eternal-novice-trap/",
        author: "bernawil",
        date: "2019-12-29T21:23:23.000Z"
      })
      .then(res => {
        const body = res.body;
        expect(body).to.contain.property("_id");
        expect(body).to.contain.property("title");
        expect(body).to.contain.property("author");
        done();
      })
      .catch((err) => done(err));
  })

  it('ok getting posts from API',  done => {
      request(app)
      .get('/posts')
      .then(res =>{
          const body = res.body;                          
          expect(body[0]).to.contain.property("author"); 
          expect(body[0]).to.contain.property("story_url");     
          done();
      })
      .catch((err) => done(err));
  })
});
