const express = require('express');
const app = express();
const postsroute = require('./routes/posts/posts');
const restclientroute = require('./routes/restclient/restclient');
const bodyParser = require('body-parser');
var cors = require('cors');
const schedule = require('node-schedule');
const request = require('request');
const Post = require('./models/Post');
const db = require('./db/index.js');

app.use(bodyParser.json());
app.use(cors());
app.use('/posts', postsroute);
app.use('/', restclientroute);

db.connect();

const j = schedule.scheduleJob('1 */1 * * *', () => {
  const currentdate = new Date();
  let posts;
  const postsArray = [];
  let hits;
  try {
    console.log(currentdate);
    request(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
      (err, body) => {
        posts = JSON.parse(body.body);
        hits = posts.hits;

        hits.forEach(element => {
          const post = new Post({
            title: element.title,
            story_title: element.story_title,
            url: element.url,
            id: element.objectID,
            story_url: element.story_url,
            description: element.description,
            author: element.author
          });
          postsArray.push(post);
          post
            .save()
            .then(data => {})
            .catch(err => {
              console.error('request failed:', err);
            });
        });

        if (err) {
          console.error('request failed:', err);
        }
      }
    );
  } catch (err) {}
});

app.listen(5000, () => {
  console.log('listening on port 5000');
});

module.exports = app;
