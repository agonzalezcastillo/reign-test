import React from 'react';
import './App.css';
import Layout from './components/Layout/Layout';

const App = () => {
  return (
    <Layout></Layout>
  );
}

export default App;
