import React from 'react';
import Box from '@material-ui/core/Box';
import classes from './Header.module.css'

const Header = (props) => {
    return(
        <Box 
        className={classes.header}        
        >
            <h1>HN News</h1>
            <h4><span>We &lt;3 hacker news!</span></h4>
        </Box>
    )
};

export default Header;
