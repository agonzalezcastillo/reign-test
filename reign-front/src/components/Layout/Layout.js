import React from 'react';
import Aux from '../../hoc/Auxiliar';
import ContentContainer from '../ContentContainer/ContentContainer';

const Layout = () =>{
    return(
        <Aux>
            <ContentContainer></ContentContainer>
        </Aux>
    )
}

export default Layout;