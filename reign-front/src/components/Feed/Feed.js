import React, { useEffect, useState } from "react";
import axios from "axios";
import StatelessFeed from './StatelessFeed';

const Feed = () => {
  const [regs, setRegs] = useState([]);

  const rowClickhandler = url => {
    window.open(url, "_blank");
  };

  const deleteClickHandler = id => {    
    axios.patch("http://localhost:5000/posts/" + id).then(res => {
      setRegs(regs.filter(p => p.id !== id));
    });
  };

  useEffect(() => {
    axios.get("http://localhost:5000/posts").then(res => {      
      setRegs(res.data);
    });
  },[]);

  return (
    <StatelessFeed 
      regs={regs} 
      rowClickhandler={rowClickhandler}
      deleteClickHandler = {deleteClickHandler}
    />
  );
};

export default Feed;
