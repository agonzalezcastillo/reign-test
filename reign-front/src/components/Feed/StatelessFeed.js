import React from "react";
import { Table, TableBody, TableCell, TableRow } from "@material-ui/core";
import classes from "./Feed.module.css";
import DeleteIcon from "@material-ui/icons/Delete";
import Moment from "react-moment";

const StatelessFeed = props => {

    const calendarStrings = {
        lastDay : '[Yesterday]',
        sameDay : 'LT',
        nextDay : '[Tomorrow at] LT',
        lastWeek : 'MMM DD',
        nextWeek : 'dddd [at] LT',
        sameElse : 'L'
    };

    return (
        <Table className={classes.feed}>
        <TableBody>
          {props.regs.map((element, index) => (                   
            <TableRow className={classes.row} key={index}>
              <TableCell onClick={e => props.rowClickhandler(e, element.story_url)}>
                <span style={{color:'#333', fontSize:'13pt'}}>{element.title ? element.title : element.story_title}</span>
                <span style={{color:'#999'}}> {"- "+element.author+" -"}</span>
              </TableCell>
              <TableCell onClick={() => props.rowClickhandler( element.story_url)}>
                <Moment calendar={calendarStrings} >{element.date}</Moment>                
              </TableCell>
              <TableCell>
                <DeleteIcon
                  onClick={() => props.deleteClickHandler(element.id)}
                  className={classes.icon}
                />
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>

    );
}

export default StatelessFeed;