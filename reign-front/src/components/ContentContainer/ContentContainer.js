import React from 'react';
import { Container } from '@material-ui/core';
import Header from '../Header/Header';
import Feed from '../Feed/Feed';

const ContentContainer = () => {
    return(
        <Container disableGutters={true}>
            <Header />
            <Feed />
        </Container>
    )
}

export default ContentContainer;