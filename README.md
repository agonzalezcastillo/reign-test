# Reign Full stack Developer
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)
### Installation

- You need to have Docker, Docker-compose installed on your PC
- clone the project from https://gitlab.com/agonzalezcastillo/reign-test.git
- move to the folder where the project was cloned 
```sh
$ cd /reign-test 
```
-start the containers with the following command
```sh
$ docker-compose up
```
-if everything went ok, on http://localhost:3000 you should see the Hacker News Feed page without any post, solve that by making the next http get request
```sh
[GET] localhost:5000
```
-that will populate the mongodb collection 'posts' with information from the requested API
-now you can refresh
```sh
 localhost:3000
```
enjoy the Hacker news Feed!


